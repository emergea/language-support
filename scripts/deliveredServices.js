var deliveredServices = new function() {

    var deliveredServicesTable;
    var selectDeliveredServiceTable;

    this.addDeliveredServicePressed = function() {
        if(servicePoint.hasValidSettings() && sessvars.state.userState == servicePoint.userState.SERVING &&
            (sessvars.state.visit.currentVisitService.deliveredServiceExists == true)) {
            util.showModal("addDeliveredServices");
            if(typeof selectDeliveredServiceTable != 'undefined') {
                selectDeliveredServiceTable.fnClearTable();
                //var enquiryMarks = WorkstationService.getMarksOfType({'markType': 'enquiry'});
                var params = servicePoint.createParams();
                params.serviceId = sessvars.state.visit.currentVisitService.serviceId;
                var deliverableServices = servicePoint.callService("getDeliverableServices", params);
                selectDeliveredServiceTable.fnAddData(deliverableServices);
                selectDeliveredServiceTable.fnAdjustColumnSizing();
            } else {
                var columns = [
/* D. ser. name.*/  {"mDataProp": "name",
                     "sClass": "firstColumn"},
/* D. ser. id */    {"bSearchable": false,
                     "bVisible": false,
                     "mDataProp": "id"}

                ];
                //marks of type
                var url = "/rest/servicepoint/branches/" + sessvars.branchId + "/services/" + sessvars.state.visit.currentVisitService.serviceId + "/deliverableServices";
                var headerCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                    nHead.style.borderBottom = "1px solid #c0c0c0";
                    nHead.getElementsByTagName('th')[0].innerHTML = jQuery.i18n.prop('info.delivered.service.name');
                };
                var rowCallback = function(nRow, aData, iDisplayIndex) {
                    /* Set onclick action */
                    nRow.onclick = deliveredServiceClicked;
                    //            nRow.style.cursor = "pointer";
                    return nRow;
                };
                selectDeliveredServiceTable = util.buildTableJson({"tableId": "selectDeliveredServiceTable", "url": url,
                    "rowCallback": rowCallback, "columns":columns, "filter": true, "headerCallback": headerCallback,
                    "scrollYHeight": "300px", "emptyTableLabel": "info.no.delivered.services.defined"});
            }
        }
    };

    var deliveredServiceClicked = function() {
        if(servicePoint.hasValidSettings()) {
            var deliveredServicesParams = servicePoint.createParams();
            deliveredServicesParams.visitId = sessvars.state.visit.id;
            deliveredServicesParams.visitServiceId = sessvars.state.visit.currentVisitService.id;
            deliveredServicesParams.deliveredServiceId = selectDeliveredServiceTable.fnGetData(this).id;
            sessvars.state = servicePoint.getState(servicePoint.callService("addDeliveredService", deliveredServicesParams));
            sessvars.statusUpdated = new Date();
            util.hideModal("addDeliveredServices");
            if(sessvars.state.servicePointState == servicePoint.servicePointState.OPEN &&
                sessvars.state.userState == servicePoint.userState.SERVING) {
//                if(typeof sessvars.markAdded == "undefined" || sessvars.markAdded == false) {
//                    sessvars.markAdded = true;
//                }
//                sessvars.visit = sessvars.state.visit;
            }
            servicePoint.updateWorkstationStatus();
        }
    };

    this.updateDeliveredServices = function() {
        if(typeof deliveredServicesTable != 'undefined') {
            deliveredServicesTable.fnClearTable();
            if(sessvars.state.visit != null && sessvars.state.visit.currentVisitService.visitDeliveredServices != null) {
                deliveredServicesTable.fnAddData(sessvars.state.visit.currentVisitService.visitDeliveredServices);
            }
        } else {
            var columns = [
/* D.serv. name */     {"sClass": "firstColumn",
                        "mDataProp": "deliveredServiceName",
                        "sDefaultContent" : null},
/* D.serv. jiql id */  {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "id",
                        "sDefaultContent" : null},
/* D.serv. orig id */  {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "deliveredServiceId",
                        "sDefaultContent" : null},
/* D.serv. outcome */  {"sClass": "middleColumn",
                        "mDataProp": "visitOutcome",
                        "sDefaultContent" : null},
/* Delivered time */   {"sClass": "lastColumn",
                        "mDataProp": "eventTime",
                        "sDefaultContent" : null},
/* D.serv. out req. */ {"bSearchable": false,
                        "bVisible": false,
                         "mDataProp": "outcomeExists"}
            ];
            var headerCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                if(nHead.getElementsByTagName('th')[0].innerHTML.length == 0) {
                    nHead.style.borderBottom = "1px solid #c0c0c0";
                    nHead.getElementsByTagName('th')[0].innerHTML = jQuery.i18n.prop('info.delivered.service.name');
                    nHead.getElementsByTagName('th')[1].innerHTML = jQuery.i18n.prop('info.delivered.service.outcome');
                    nHead.getElementsByTagName('th')[2].innerHTML = jQuery.i18n.prop('info.delivered.service.time');
                }
            };
            var rowCallback = function(nRow, aData, iDisplayIndex) {
//                var markTimeDataIndex = "eventTime";
//                if a matter code is required, make sure the used data is end time
//                if(sessvars.forceMark) {
//                    markTimeDataIndex = "end";
//                }
//                manually parse date from server and feed to javascript date
//                if(null != aData[markTimeDataIndex]) {
//                    var splitDateTime = aData[markTimeDataIndex].split("T");
    //                yyyy-mm-dd
//                    var date = splitDateTime[0].split("-");
    //                hh:mmm:ss
//                    var time = splitDateTime[1].split(":");
    //                poor IE. We need to create date using format: yyyy, mm-1, dd, hh, mm, ss
//                    var markTime = new Date(date[0], date[1]-1, date[2], time[0], time[1], time[2]);
//                    var formattedTime =
//                        (markTime.getHours() < 10 ? "0" : "") + markTime.getHours() + ":" +
//                        (markTime.getMinutes() < 10 ? "0" : "") + markTime.getMinutes() + ":" +
//                        (markTime.getSeconds() < 10 ? "0" : "") + markTime.getSeconds();
//                    $('td:eq(1)', nRow).html(formattedTime);
//                }
//                return nRow;
                var outcomeDataIndex = "visitOutcome";
                if(aData["outcomeExists"] == true || null != aData[outcomeDataIndex]) {
                    var html;
                    html = $('<select id="outcomeList_' + iDisplayIndex + '"' + '><option value="-1"></option></select>');
                    $("option[value='-1']", html).text(translate.msg("info.choose.outcome.for.delivered.service")).attr('disabled', 'disabled');
                    var params = servicePoint.createParams();
                    params.serviceId = sessvars.state.visit.currentVisitService.serviceId;
                    params.deliveredServiceId = aData["deliveredServiceId"];
                    var possibleOutcomes = servicePoint.callService("getOutcomesForDeliveredService", params);
                    $.each(possibleOutcomes, function(i, outcome) {
                        html.append($("<option></option>")
                        .prop("value", outcome.code)
                            .text(outcome.name))
                    });
                    if(null != aData[outcomeDataIndex]) {
                        html.val(aData[outcomeDataIndex].outcomeCode);
                    } else {
                        html.val("-1");
                    }
                    html.change(function() {
                        if($(this).val() != -1) {
                            var params = servicePoint.createParams();
                            params.visitId = sessvars.state.visit.id;
                            var nTr = $(this).closest("tr").get(0);
                            var aData = deliveredServicesTable.fnGetData(nTr);
                            params.visitDeliveredServiceId = parseInt(aData["id"]);
                            params.outcomeCode = $(this).val();
                            sessvars.state = servicePoint.getState(servicePoint.callService("setOutcomeForDeliveredService", params));
                            servicePoint.updateWorkstationStatus();
                        }
                    });
                    $('td:eq(1)', nRow).html(html);
                }
            };
            deliveredServicesTable = $('#deliveredServices').dataTable( {
                "bDestroy": true,
                "oLanguage": {
                    "sEmptyTable": translate.msg("info.no.delivered.services"),
                    "sInfo": "",
                    "sInfoEmpty": "",
                    "sZeroRecords": ""
                },
                "bFilter": false,
                "fnRowCallback": rowCallback,
                "fnHeaderCallback": headerCallback,
                "bLengthChange": false,
                "bProcessing": true,
                "bPaginate": false,
                "aoColumns": columns,
                "sScrollX": "95%",
                "sScrollY": "158px",
                "aaData": (sessvars.state.visit != null &&
                    sessvars.state.visit.currentVisitService != null &&
                    sessvars.state.visit.currentVisitService.visitDeliveredServices !== null ?
                    sessvars.state.visit.currentVisitService.visitDeliveredServices : null)
            });
            $(window).bind('resize', function () {
                deliveredServicesTable.fnAdjustColumnSizing();
            } );
        }
        $(document).ready(function() {
            var sorting = [[4, 'desc'], [1, 'desc']];
            deliveredServicesTable.fnSort(sorting);
        });
    };

    this.cancelAddDeliveredServices = function() {
        util.hideModal("addDeliveredServices");
    };

    this.hideAddDeliveredServices = function() {
        util.hideModal("addDeliveredServices");
    };

    this.clearTable = function() {
        util.clearTable(deliveredServicesTable);
    };

};