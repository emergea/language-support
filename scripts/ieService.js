
var spService = (function($) {

	var _user;
	var _sysInfo;
	var _profiles;
	var _branches;
	var _serviceData;
	var _variableData;
	
	function _getUser() {
		$.ajax({
			type: 'GET',
			url: '/rest/servicepoint/user',
			dataType: 'json',
			async: false,
			cache: false,
			success: function(user) {
				_user = user;
			},
			error: function(xhr, type) {
				_user = null;
			}
		});
	}
	
	
	function _logout() {
		$.ajax({
			type: 'PUT',
			url: '/rest/servicepoint/logout',
			dataType: 'json',
			async: false,
			success: function() {
				
			},
			error: function(xhr, type) {
			
			}
		});
	}

	function _transferVisitToQueue(params) {
		$.ajax({
			type: 'PUT',
			url: '/rest/servicepoint/branches/' + params.branchId + '/queues/' + params.queueId + '/visits/',
			data: params.json,
			contentType: 'application/json',
			async: false,
			success: function() {
				
			},
			error: function(xhr, type) {
				
			}
		});
	}
	
		function _transferVisitToServicePointPool(params) {
		$.ajax({
			type: 'PUT',
			url: '/rest/servicepoint/branches/' + params.branchId + '/servicePoints/' + params.servicePointId + '/visits/',
			data: params.json,
			contentType: 'application/json',
			async: false,
			success: function() {
				
			},
			error: function(xhr, type) {
				
			}
		});
	}

	function _transferVisitToUserPool(params) {
		$.ajax({
			type: 'PUT',
			url: '/rest/servicepoint/branches/' + params.branchId + '/users/' + params.userId + '/visits/',
			data: params.json,
			contentType: 'application/json',
			async: false,
			success: function() {
				
			},
			error: function(xhr, type) {
				
			}
		});
	}	
	
	function _setProfile(params) {
		$.ajax({
			type: 'PUT',
			url:'/rest/servicepoint/branches/' + params.branchId + '/users/' + params.userName + '/workProfile/' + params.workProfileId,
			dataType: 'json',
			async: false,
			success: function() {
				
			},
			error: function(xhr, type) {
				
			}
		});
	}
	
	return {
	
		getCurrentUser : function() {
			_getUser();
			return _user;
		},
		
		logout : function() {
			_logout();
		},
		
		transferVisitToQueue : function(params) {
			_transferVisitToQueue(params);
		},
		
		transferVisitToServicePointPool : function(params) {
			_transferVisitToServicePointPool(params);
		},
		
		transferVisitToUserPool : function(params) {
			_transferVisitToUserPool(params);
		},
		setProfile : function(params) {
			_setProfile(params);
		}		
	};

})(jQuery);