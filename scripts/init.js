var init = new function() {

    var useAdvancedFailoverDetection = true;

    this.init = function() {
        // we need to override the default resteasy constructor in order to get synchronous behaviour with callbacks
        var oldprototype = REST.Request.prototype;
		REST.Request = function () {
            REST.log("Creating new Request");
            this.uri = null;
            this.method = "GET";
            this.username = null;
            this.password = null;
            this.acceptHeader = "*/*";
            this.contentTypeHeader = null;
            this.async = false;
            this.queryParameters = [];
            this.matrixParameters = [];
            this.formParameters = [];
            this.cookies = [];
            this.headers = [];
            this.entity = null;
        };
		REST.Request.prototype = oldprototype;

        // dwr.engine.setErrorHandler(qevents.elkErrorHandler);
        // custom status handler is defined in scripts.js
        // only override this if advanced failover detection is desired
        qevents.init(false, typeof useAdvancedFailoverDetection !== 'undefined' && useAdvancedFailoverDetection ?
            servicePoint.cometDWSPollStatusHandler : "");
//        if (typeof useAdvancedFailoverDetection !== 'undefined' && useAdvancedFailoverDetection) {
//            qevents.setConnectionStatusChangeCallback(servicePoint.cometDWSPollStatusHandler);
//        }
        //parse text areas and impose max length
        var txts = document.getElementsByTagName('TEXTAREA');
        for(var i = 0, l = txts.length; i < l; i++) {
            if(/^[0-9]+$/.test(txts[i].getAttribute("maxlength"))) {
                var func = function() {
                    var len = parseInt(this.getAttribute("maxlength"), 10);

                    if(this.value.length > len) {
                        this.value = this.value.substr(0, len);
                        return false;
                    }
                };
                txts[i].onkeyup = func;
                txts[i].onblur = func;
            }
        }

//        if(typeof sessvars.currentUser === "undefined" || null == sessvars.currentUser || sessvars.currentUser == "" ) {
            sessvars.currentUser = ServicePointService.getCurrentUser();
//        }

        if(typeof sessvars.systemInformation == "undefined" || sessvars.systemInformation== "" || null == sessvars.systemInformation) {
            sessvars.systemInformation = ServicePointService.getSystemInformation();
        }
        jQuery.i18n.properties({
            name:'workstationTerminalMessages',
            path:'lang/',
            mode:'map',
            language: sessvars.currentUser.locale == sessvars.systemInformation.defaultLanguage ? " " : sessvars.currentUser.locale,
            callback : function () {
                i18n.i18nPage();
            }
        });
        //check for RTL rendering
        try{
            if(sessvars.currentUser.direction == "rtl") {
                var styles_rtl_file = document.createElement("link");
                styles_rtl_file.setAttribute("href", "css/styles_rtl.css");
                styles_rtl_file.setAttribute("type", "text/css");
                styles_rtl_file.setAttribute("rel", "stylesheet");
                if(typeof styles_rtl_file != "undefined") {
                    document.getElementsByTagName("head")[0].appendChild(styles_rtl_file);
                }
                var orch_rtl_ref = document.createElement("link");
                orch_rtl_ref.setAttribute("href", "/css/rtl.css");
                orch_rtl_ref.setAttribute("type", "text/css");
                orch_rtl_ref.setAttribute("rel", "stylesheet");
                if(typeof orch_rtl_ref != "undefined") {
                    document.getElementsByTagName("head")[0].appendChild(orch_rtl_ref);
                }
            }
        } catch(e) {
            //nothing found; rtl prop not set
        }

        //make stuff draggable
        $(".branchForm, .logoutForm, .customerForm, .confirmCounterHijackingForm, .confirmCustomer, .transferForm").draggable({
            containment: "window",
            handle: "h2",
            cancel: "a"
        });
		// chrome hack to allow for proper dragging, QP-892
        $('.branchForm, .logoutForm, .customerForm, .confirmCounterHijackingForm, .confirmCustomer, .transferForm').each(function(i){
			var x = ($(window).width() - $(this).width()) / 2;
			$(this).css({position:"absolute",top:100,left:x});
        });

        initSessvars();
        // only used in case of errors in qevents.subscribe(...)
//        qevents.setErrorMessageHandler(util.showCometDError());
        //localize date picker
        customer.init();
        servicePoint.init();
    };

    var initSessvars = function() {
		var userState = servicePoint.getState(servicePoint.callService("getUserStatus", {}));
		if(sessvars.state == undefined) {
			if(userState.servicePointId != undefined) {
				//console.log('****** multiple sessions detected *****');
				util.showModal("alreadyLoggedInWindow");
				$(document).ready(function() {
					document.getElementById("alreadyLoggedInMessageDiv").value = jQuery.i18n.prop('label.information');
				 	document.getElementById("alreadyLoggedInConfirmLink").value = jQuery.i18n.prop('button.ok');
				});
			}
		}
        sessvars.state = userState;
        sessvars.statusUpdated = new Date();
        if(typeof sessvars.state !== 'undefined' && sessvars.state != null &&
            typeof sessvars.state.branchId !== "undefined" && sessvars.state.branchId != null &&
            typeof sessvars.state.servicePointId !== "undefined" && sessvars.state.servicePointId != null &&
            typeof sessvars.state.workProfileId !== "undefined" && sessvars.state.workProfileId != null) {
            servicePoint.storeSettingsInSession(sessvars.state);
        } else {
            // something is not valid, everything is invalid. Scenario: New configuration has been published
            servicePoint.resetSettings();
        }
    }
};