var outcome = new function() {

    var outcomeTable;
    var selectOutcomeTable;

    this.addOutcomePressed = function() {
        if(servicePoint.hasValidSettings() && (sessvars.state.userState == servicePoint.userState.SERVING &&
            (sessvars.state.visit.currentVisitService.outcomeExists == true))) {
            util.showModal("addOutcomes");
            if(typeof selectOutcomeTable != 'undefined') {
                selectOutcomeTable.fnClearTable();
                //var enquiryMarks = WorkstationService.getMarksOfType({'markType': 'enquiry'});
                var params = servicePoint.createParams();
                params.serviceId = sessvars.state.visit.currentVisitService.serviceId;
                var outcomes = servicePoint.callService("getOutcomesForService", params);
                selectOutcomeTable.fnAddData(outcomes);
            } else {
                var columns = [
/* Outcome name */  {"sClass": "firstColumn",
                     "mDataProp": "name"},
/* Outcome id */    {"bSearchable": false,
                     "bVisible": false,
                     "mDataProp": "code"}
                ];
                var url = "/rest/servicepoint/branches/" + sessvars.branchId + "/services/" + sessvars.state.visit.currentVisitService.serviceId + "/outcomes";
                var headerCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                    nHead.style.borderBottom = "1px solid #c0c0c0";
                    nHead.getElementsByTagName('th')[0].innerHTML = jQuery.i18n.prop('info.outcome.name');
                };
                var rowCallback = function(nRow, aData, iDisplayIndex) {
                    /* Set onclick action */
                    nRow.onclick = outcomeClicked;
                    //            nRow.style.cursor = "pointer";
                    return nRow;
                };
                selectOutcomeTable = util.buildTable("selectOutcomeTable", url, rowCallback, columns, true, headerCallback, true);
            }
        }
    };

    this.selectOutcome = function(val) {
        if(val != -1 && servicePoint.hasValidSettings()) {
            var outcomeForServiceParams = servicePoint.createParams();
            outcomeForServiceParams.visitId = sessvars.state.visit.id;
            outcomeForServiceParams.visitServiceId = sessvars.state.visit.currentVisitService.id;
            outcomeForServiceParams.outcomeCode = val;
            sessvars.state = servicePoint.getState(servicePoint.callService("setOutcomeForService", outcomeForServiceParams));
            sessvars.statusUpdated = new Date();
            if(sessvars.state.servicePointState == servicePoint.servicePointState.OPEN && sessvars.state.userState == servicePoint.userState.SERVING) {
//                if(typeof sessvars.markAdded == "undefined" || sessvars.markAdded == false) {
//                    sessvars.markAdded = true;
//                }
                sessvars.visit = sessvars.state.visit;

            }
            servicePoint.updateWorkstationStatus();
        }
    };

    var outcomeClicked = function() {
        if(servicePoint.hasValidSettings()) {
            var outcomeForServiceParams = servicePoint.createParams();
            outcomeForServiceParams.visitId = sessvars.state.visit.id;
            outcomeForServiceParams.visitServiceId = sessvars.state.visit.currentVisitService.id;
            outcomeForServiceParams.outcomeCode = selectOutcomeTable.fnGetData(this).code;
            sessvars.state = servicePoint.getState(servicePoint.callService("setOutcomeForService", outcomeForServiceParams));
            sessvars.statusUpdated = new Date();
            outcome.hideAddOutcomes();
            if(sessvars.state.servicePointState == servicePoint.servicePointState.OPEN && sessvars.state.userState == servicePoint.userState.SERVING) {
//                if(typeof sessvars.markAdded == "undefined" || sessvars.markAdded == false) {
//                    sessvars.markAdded = true;
//                }
                sessvars.visit = sessvars.state.visit;

            }
            servicePoint.updateWorkstationStatus();
        }
    };

    this.updateOutcomes = function() {
        var outcomeSelect = $("#selectOutcome");
        util.clearSelect(outcomeSelect);
        if(sessvars.state.userState == servicePoint.userState.SERVING && typeof sessvars.state.visit !== "undefined" &&
            sessvars.state.visit != null) {
            var params = servicePoint.createParams();
            params.serviceId = sessvars.state.visit.currentVisitService.serviceId;
            var outcomes = servicePoint.callService("getOutcomesForService", params);
            if(typeof outcomes !== 'undefined' && outcomes != null && outcomes.length > 0) {
                outcomeSelect.removeAttr('disabled');
                util.populateSelect(outcomes, outcomeSelect, "code");
                if(null != sessvars.state.visit.currentVisitService.visitOutcome) {
                    outcomeSelect.val(sessvars.state.visit.currentVisitService.visitOutcome.outcomeCode);
                } else {
                    outcomeSelect.val("-1");
                }
            } else {
                outcomeSelect.val("-1");
                outcomeSelect.attr('disabled', '');
            }
        } else {
            outcomeSelect.val("-1");
            outcomeSelect.attr('disabled', '');
        }
    };

    this.updateOutcomesTable = function() {
        if(typeof outcomeTable != 'undefined') {
            outcomeTable.fnClearTable();
            //var marksForTicket = WorkstationService.getVisitMarks({'visitId': sessvars.state.trackNo * 10000 + sessvars.branchId});
//                var marksForTicket = servicePoint.callService("getVisitMarks",
//                    {'visitId': sessvars.state.trackNo * 10000 + sessvars.branchId});
            if(null != sessvars.state.visit && null != sessvars.state.visit.currentVisitService.visitOutcome) {
                outcomeTable.fnAddData([sessvars.state.visit.currentVisitService.visitOutcome]);
            }
        } else {
            var columns = [
/* O. name */           {"sClass": "firstColumn",
                         "mDataProp": "outcomeName",
                         "sDefaultContent" : null},
/* O. id */             {"bSearchable": false,
                         "bVisible": false,
                         "mDataProp": "id",
                         "sDefaultContent" : null},
/* O. event. t*/        {"sClass": "lastColumn",
                         "mDataProp": "eventTime",
                         "sDefaultContent" : null},
/* O. orig. id */       {"bSearchable": false,
                         "bVisible": false,
                         "mDataProp": "outcomeId",
                         "sDefaultContent" : null}
            ];
            var headerCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                if(nHead.getElementsByTagName('th')[0].innerHTML.length == 0) {
                    nHead.style.borderBottom = "1px solid #c0c0c0";
                    nHead.getElementsByTagName('th')[0].innerHTML = jQuery.i18n.prop('info.outcome.name');
                    nHead.getElementsByTagName('th')[1].innerHTML = jQuery.i18n.prop('info.outcome.time');
//                        if(sessvars.forceMark) {
//                            nHead.getElementsByTagName('th')[1].innerHTML = jQuery.i18n.prop('info.mark.end.time');
//                        } else {
//                            nHead.getElementsByTagName('th')[1].innerHTML = jQuery.i18n.prop('info.mark.start.time');
//                        }
                }
            };
            outcomeTable = $('#outcomes').dataTable( {
                "bDestroy": true,
                "aaSorting": [[1, 'desc']],
                "oLanguage": {
                    "sEmptyTable": translate.msg("info.no.outcomes"),
                    "sInfo": "",
                    "sInfoEmpty": "",
                    "sZeroRecords": ""
                },
                "bFilter": false,
                "bLengthChange": false,
                "fnHeaderCallback": headerCallback,
                "bProcessing": true,
                "bPaginate": false,
                "aoColumns": columns,
                "sScrollX": "95%",
                "sScrollY": "115px",
                "aaData": (sessvars.state.visit != null &&
                    sessvars.state.visit.currentVisitService != null &&
                    sessvars.state.visit.currentVisitService.visitOutcome != null ?
                    [sessvars.state.visit.currentVisitService.visitOutcome] : null)
            });
            //marksTable = util.buildTable("marks", url, rowCallback, columns, false, headerCallback);
        }
        $(document).ready(function() {
            var sorting = [[2, 'desc']];
            outcomeTable.fnSort(sorting);
        });
    };

    this.cancelAddOutcomes = function() {
        util.hideModal("addOutcomes");
    };

    this.hideAddOutcomes = function() {
        util.hideModal("addOutcomes");
    };

    this.clearTable = function() {
        util.clearTable(outcomeTable);
    };

    this.clearOutcome = function() {
        util.clearSelect($("#selectOutcome"));
    };

};