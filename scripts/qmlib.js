// ---------------------------------------------------------------------------------------------------------------------------------
// Version : 4.1.03
// 
// Updates : 4.1.02 - 20120518 BvD 
//                  - Created putCallBackRequest which returns the info on put
//           4.1.03 - 20130326 BvD
//                  - Solved memory leak in all requests           
// ---------------------------------------------------------------------------------------------------------------------------------


function _getXmlHttpRequest() {
	try {
		// Firefox, Opera 8.0+, Safari
		return new XMLHttpRequest();
	} catch (e) {
		// Internet Explorer
		try {
			return new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				return new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				alert("Your browser does not support AJAX!");
				return null;
			}
		}
	}
}
/*
function getRequest(path, callback) {
	var xhr = getXmlHttpRequest();
	xhr.onreadystatechange = function() {
		if(this.readyState == 4) {
			if(this.status == 200) {
				var json = eval('(' + this.responseText + ')');
				callback(json);
			} else {
				showError(this.responseText);
			}
		}
	};
	xhr.open("GET", path,true);
	xhr.setRequestHeader("Accept", "application/json");
	xhr.send(null);
	xhr = null;
}

function getRequest(path, callback, user, pwd) {
	var xhr = getXmlHttpRequest();
	xhr.onreadystatechange = function() {
		if(this.readyState == 4) {
			if(this.status == 200) {
				var json = eval('(' + this.responseText + ')');
				callback(json);
			} else {
				showError(this.responseText);
			}
		}
	};
	xhr.open("GET", path,true, user, pwd);
	xhr.setRequestHeader("Accept", "application/json");
	xhr.send(null);
	xhr = null;
}

function postRequest(path, json) {
	var xhr = getXmlHttpRequest();
	xhr.onreadystatechange = function() {
		if(this.readyState == 4) {
			if(this.status == 200 || this.status == 204) {
				return;
			} else {
				showError(this.responseText);
			}
		}
	};
	xhr.open("POST", path, true);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.setRequestHeader("Accept", "application/json");
	xhr.send(json);
	xhr = null;
}

function postRequest(path, json, user, pwd) {
	var xhr = getXmlHttpRequest();
	xhr.onreadystatechange = function() {
		if(this.readyState == 4) {
			if(this.status == 200 || this.status == 204) {
				return;
			} else {
				showError(this.responseText);
			}
		}
	};
	xhr.open("POST", path, true, user, pwd);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.setRequestHeader("Accept", "application/json");
	xhr.send(json);
	xhr = null;
}

function putRequest(path) {
	var xhr = getXmlHttpRequest();
	xhr.onreadystatechange = function() {
		if(this.readyState == 4) {
			if(this.status == 200 || this.status == 204) {
				return;
			} else {
				//showError(this.responseText);
			}
		}
	};
	
	xhr.open("PUT", path, true);
	//xhr.setRequestHeader("Accept", "application/json");
	xhr.send(null);
	xhr = null;
}

function putRequest(path, user, pwd) {
	var xhr = getXmlHttpRequest();
	xhr.onreadystatechange = function() {
		if(this.readyState == 4) {
			if(this.status == 200 || this.status == 204) {
				return;
			} else {
				//showError(this.responseText);
			}
		}
	};
	
	xhr.open("PUT", path, true, user, pwd);
	//xhr.setRequestHeader("Accept", "application/json");
	xhr.send(null);
	xhr = null;
}

function putCallBackRequest(path, callback, useInfo, user, pwd) {
	var xhr = getXmlHttpRequest();
	xhr.onreadystatechange = function() {
		if(this.readyState == 4) {
			if(this.status == 200 || this.status == 204) {
				if (useInfo == 1 ) {
					var json = eval('(' + this.responseText + ')');
					callback(json);
				} else {
					callback
				}
			} else {
				showError(this.responseText);
			}
		}
	};
	xhr.open("PUT", path, true, user, pwd);
	xhr.setRequestHeader("Accept", "application/json");
	xhr.send(null);
	xhr = null;
}

*/

function putJsonRequest(path, json, user, pwd) {
	var xhr = _getXmlHttpRequest();
	xhr.onreadystatechange = function() {
		if(this.readyState == 4) {
			if(this.status == 200 || this.status == 204 || this.status == 1223) {
				return;
			} else {
			//	showError(this.responseText);
			}
		}
	};
	xhr.open("PUT", path, true, user, pwd);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.setRequestHeader("Accept", "application/json");
	xhr.send(json);
	xhr = null;
}

function putJsonRequestNewFunc(path, newFunc, json, user, pwd) {
	var xhr = _getXmlHttpRequest();
	xhr.onreadystatechange = function() {
		if(this.readyState == 4) {
			if(this.status == 200 || this.status == 204 || this.status == 1223) {
				newFunc();
			} else {
			//	showError(this.responseText);
			}
		}
	};
	xhr.open("PUT", path, true, user, pwd);
	xhr.setRequestHeader("Content-type", "application/json");
	xhr.setRequestHeader("Accept", "application/json");
	xhr.send(json);
	xhr = null;
}


REST.Request.prototype = {
		uri : null,
		method : "GET",
		username : null,
		password : null,
		acceptHeader : "*/*",
		contentTypeHeader : null,
		async : true,
		queryParameters : [],
		matrixParameters : [],
		cookies : [],
		headers : [],
		entity : null,
		execute : function(callback){
			var request = _getXmlHttpRequest();
			var url = this.uri;
			var restRequest = this;
			for(var i=0;i<this.matrixParameters.length;i++){
				url += ";" + REST.encodePathParamName(this.matrixParameters[i][0]);
				url += "=" + REST.encodePathParamValue(this.matrixParameters[i][1]);
			}
			for(var i=0;i<this.queryParameters.length;i++){
				if(i == 0)
					url += "?";
				else
					url += "&";
				url += REST.encodeQueryParamNameOrValue(this.queryParameters[i][0]);
				url += "=" + REST.encodeQueryParamNameOrValue(this.queryParameters[i][1]);
			}
			for(var i=0;i<this.cookies.length;i++){
				document.cookie = escape(this.cookies[i][0]) 
					+ "=" + escape(this.cookies[i][1]);
			}
			request.open(this.method, url, this.async, this.username, this.password);
			var acceptSet = false;
			var contentTypeSet = false;
			for(var i=0;i<this.headers.length;i++){
				if(this.headers[i][0].toLowerCase() == 'accept')
					acceptSet = this.headers[i][1];
				if(this.headers[i][0].toLowerCase() == 'content-type')
					contentTypeSet = this.headers[i][1];
				request.setRequestHeader(REST.encodeHeaderName(this.headers[i][0]),
						REST.encodeHeaderValue(this.headers[i][1]));
			}
			if(!acceptSet)
				request.setRequestHeader('Accept', this.acceptHeader);
			if(this.entity && !contentTypeSet && this.contentTypeHeader){
				contentTypeSet = this.contentTypeHeader;
				request.setRequestHeader('Content-Type', this.contentTypeHeader);
			}
			// we use this flag to work around buggy browsers
			var gotReadyStateChangeEvent = false;
			if(callback){
				request.onreadystatechange = function() {
					gotReadyStateChangeEvent = true;
					REST.log("Got readystatechange");
					REST._complete(this, callback);
				};
			}
			var data = this.entity;
			if(this.entity){
//				if(this.entity instanceof Element){
//					if(!contentTypeSet || REST._isXMLMIME(contentTypeSet))
//						data = REST.serialiseXML(this.entity);
				//}else if(this.entity instanceof Document){
				//	if(!contentTypeSet || REST._isXMLMIME(contentTypeSet))
				//		data = this.entity;
				if(this.entity instanceof Object){
					if(!contentTypeSet || contentTypeSet == "application/json")
						data = JSON.stringify(this.entity);
				}
			}
			REST.log("Content-Type set to "+contentTypeSet);
			REST.log("Entity set to "+data);
			request.send(data);
			// now if the browser did not follow the specs and did not fire the events while synchronous,
			// handle it manually
			if(!this.async && !gotReadyStateChangeEvent && callback){
				REST.log("Working around browser readystatechange bug");
				REST._complete(request, callback);
			}
		},
		setAccepts : function(acceptHeader){
			REST.log("setAccepts("+acceptHeader+")");
			this.acceptHeader = acceptHeader;
		},
		setCredentials : function(username, password){
			this.password = password;
			this.username = username;
		},
		setEntity : function(entity){
			REST.log("setEntity("+entity+")");
			this.entity = entity;
		},
		setContentType : function(contentType){
			REST.log("setContentType("+contentType+")");
			this.contentTypeHeader = contentType;
		},
		setURI : function(uri){
			REST.log("setURI("+uri+")");
			this.uri = uri;
		},
		setMethod : function(method){
			REST.log("setMethod("+method+")");
			this.method = method;
		},
		setAsync : function(async){
			REST.log("setAsync("+async+")");
			this.async = async;
		},
		addCookie : function(name, value){
			this.cookies.push([name, value]);
		},
		addQueryParameter : function(name, value){
			this.queryParameters.push([name, value]);
		},
		addMatrixParameter : function(name, value){
			this.matrixParameters.push([name, value]);
		},
		addHeader : function(name, value){
			this.headers.push([name, value]);
		}
}