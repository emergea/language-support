var queues = new function() {

    var queuesTable;
    var ticketsTable;

    /*
     * keepCalling should be set to true to have this function call itself every 30 secs.
     * sessvars.queueTimer can be used to stop the call
     */
    this.updateQueues = function(keepCalling) {
        if(!servicePoint.getWorkstationOffline() && servicePoint.hasValidSettings()) {
            if(typeof queuesTable !== 'undefined') {
                queuesTable.fnClearTable();
                var queuesData = servicePoint.callServiceURL("/rest/servicepoint/branches/" + sessvars.branchId + "/queues");
                queuesTable.fnAddData(queuesData);
            } else {
                var columns = [
                    /* Queue name */        {"sClass": "firstColumn",
                        "mDataProp": "name",
                        "sDefaultContent" : null},
                    /* Queue id */          {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "id",
                        "sDefaultContent" : null},
//        /* Queue description */ {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "description"},
//        /* Queue branch id */   {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "branchId"},
//        /* Queue QL num */      {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "queueLogicNumber"},
                    /* Queue waiting time */{"sClass": "middleColumn",
                        "mDataProp": "waitingTime",
                        "sDefaultContent" : null},
                    /* Queue waiting num */ {"sClass": "lastColumn",
                        "mDataProp": "customersWaiting",
                        "sDefaultContent" : null}
//        /* Queue letter */      {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "letter"},
//        /* Queue est wait */    {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "estimatedWait"}
                ];
                var headerCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                    nHead.style.borderBottom = "1px solid #c0c0c0";
                    nHead.getElementsByTagName('th')[0].innerHTML = jQuery.i18n.prop('info.queue.name.short');
                    nHead.getElementsByTagName('th')[1].innerHTML = jQuery.i18n.prop('info.queue.waiting.time.short');
                    nHead.getElementsByTagName('th')[2].innerHTML = jQuery.i18n.prop('info.queue.waiting.short');
                };
                var url = "/rest/servicepoint/branches/" + sessvars.branchId + "/queues";
                var rowCallback = function(nRow, aData, iDisplayIndex) {
	
                    if(sessvars.state.servicePointState == servicePoint.servicePointState.OPEN &&
                        !(servicePoint.isOutcomeOrDeliveredServiceNeeded() /*&& sessvars.forceMark && !hasMark()*/)) {
                        var queueName = $('td:eq(0)', nRow).text();
                        $('td:eq(0)', nRow).empty().append("<span class=\"queueName\" " +
                            "title=\"" + jQuery.i18n.prop("action.title.queue.click") + "\">" + queueName + "</span>");

							
                        $('td:eq(0) > span.queueName', nRow).click(function() {
                            queueClicked(nRow);
							
						
							
                        });
                    } else {
                        $('td:eq(0)', nRow).addClass("queueNameDisabled");
                    }
					
                    $('td:eq(1)', nRow).html(util.formatIntoHHMM(parseInt(aData.waitingTime)));
					
					if ( skillEnabled == true && sessvars.skillQueues != undefined ) {
						var queuesList = sessvars.skillQueues.split(",");	
						var notInList = true ;	

						for ( i=0; i < queuesList.length; i++ ) {
							if ( queuesList[i] == aData.id ) {
								notInList = false;
							}
						}
						
						if (notInList == true ) {	
							$('td:eq(0)', nRow).hide();
							$('td:eq(1)', nRow).hide();
							$('td:eq(2)', nRow).hide();
						}
					}
                    return nRow;
                };
                queuesTable = util.buildTableJson({"tableId": "queues", "url": url, "rowCallback": rowCallback,
                    "columns": columns, "filter": false, "headerCallback": headerCallback, "emptyTableLabel": "info.queues.none"});
            }
        }
        if(keepCalling) {
            sessvars.queueTimer = setTimeout("queues.updateQueues(true)", queueRefeshTime*1000);
        }
    };

    var queueClicked = function(rowClicked) {
        if(servicePoint.hasValidSettings() && sessvars.state.servicePointState == servicePoint.servicePointState.OPEN &&
            !(servicePoint.isOutcomeOrDeliveredServiceNeeded() /*&& sessvars.forceMark && !hasMark()*/)) {
            sessvars.clickedQueueId = queuesTable.fnGetData(rowClicked).id; //ql queue id
            util.showModal("ticketsDialogue");
            if(typeof ticketsTable !== 'undefined') {
                //empty the tickets table and populate with new data from server if table is not created
                ticketsTable.fnClearTable();
                var params = {};
                params.branchId = sessvars.branchId;
                params.queueId = sessvars.clickedQueueId;
                //var tickets = WorkstationService.getVisitsInQueue(params);
                var tickets = servicePoint.callService("getVisitsInQueue", params);
                ticketsTable.fnAddData(tickets);
                ticketsTable.fnAdjustColumnSizing();
            } else {
                var columns = [
                    /* Id */                {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "visitId"},

                    /* Ticket id */         {"sClass": "firstColumn",
                        "mDataProp": "ticketId"},

                    /* Actions */      {"sClass": "middleColumn",
                        "mData": null,
                        "sDefaultContent": ""},
                    /* Waiting time */      {"sClass": "lastColumn",
                        "mDataProp": "waitingTime"}

                ];
                var headerCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                    nHead.style.borderBottom = "1px solid #c0c0c0";
                    nHead.getElementsByTagName('th')[0].innerHTML = jQuery.i18n.prop('info.queue.tickets');
                    nHead.getElementsByTagName('th')[1].innerHTML = jQuery.i18n.prop('info.queue.tickets.actions');
                    nHead.getElementsByTagName('th')[2].innerHTML = jQuery.i18n.prop('info.queue.waiting.time');
                };
                var url = "/rest/servicepoint/branches/" + sessvars.branchId + "/queues/" + sessvars.clickedQueueId + "/visits";
                var rowCallback = function(nRow, aData, iDisplayIndex) {
                    if($('td:eq(0)', nRow).find('span').length == 0) {
                        //format ticket number
                        $('td:eq(0)', nRow).html("<span class='ticketNumSpan'>" + aData.ticketId + "</span>");
						if ( buttonTransferFromQueueEnabled  == true ) {						
							$('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"transferTicket\" title=\"" + jQuery.i18n.prop("action.title.transfer") + "\"></a></span>");
						}
						if ( buttonRemoveFromQueueEnabled == true ) {
							$('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"removeTicket\" title=\"" + jQuery.i18n.prop("action.title.remove") + "\"></a></span>");
						}
						if ( buttonCallFromQueueEnabled  == true ) {						
							$('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"callTicket\" title=\"" +  jQuery.i18n.prop("action.title.call.ticket")  + "\"></a></span>");
						}
                        var formattedTime = util.formatIntoHHMM(parseInt(aData.waitingTime));
                    }
                    $('td:eq(2)', nRow).html(formattedTime);
                    $(nRow).addClass("");
                    return nRow;
                };
				
                //create new table since not defined
                ticketsTable = util.buildTableJson({"tableId": "tickets", "url": url, "rowCallback": rowCallback,
                    "columns": columns, "filter": false, "headerCallback": headerCallback, "scrollYHeight": "300px",
                    "emptyTableLabel": "info.queue.tickets.empty"});
            }

            //kill old event handlers
			if ( buttonTransferFromQueueEnabled  == true ) {
				$('tbody td span a.transferTicket', $('#tickets')).die('click');
			}
			if ( buttonRemoveFromQueueEnabled  == true ) {
				$('tbody td span a.removeTicket', $('#tickets')).die('click');
			}
			if ( buttonCallFromQueueEnabled  == true ) {
				$('tbody td span a.callTicket', $('#tickets')).die('click');
			}
	
			if ( buttonTransferFromQueueEnabled  == true ) {	
				$('tbody td span a.transferTicket', $('#tickets')).live('click', function() {
					var nTr = $(this).closest("tr").get(0);
					var aData = ticketsTable.fnGetData(nTr);
					transfer.transferTicketToQueueClicked(aData);
					util.hideModal('ticketsDialogue');
					return false;
				});
			}			

			if ( buttonRemoveFromQueueEnabled == true ) {
				$('tbody td span a.removeTicket', $('#tickets')).live('click', function() {
					var nTr = $(this).closest("tr").get(0);
					var aData = ticketsTable.fnGetData(nTr);
					removeTicketClicked(aData);
					util.hideModal('ticketsDialogue');
					return false;
				});
			}

			if ( buttonCallFromQueueEnabled  == true ) {			
				$('tbody td span a.callTicket', $('#tickets')).live('click', function() {
					var nTr = $(this).closest("tr").get(0);
					var aData = ticketsTable.fnGetData(nTr);
					callTicketClicked(aData);
					util.hideModal('ticketsDialogue');
					return false;
				});
			}
            $("#ticketListHeader").empty();
            $("#ticketListHeader").html(
                '<a href=\"#\" class=\"closeButton\" onclick="javascript:util.hideModal(\'ticketsDialogue\')"></a>' +
                    translate.msg("info.list.of.tickets.in", [queuesTable.fnGetData(rowClicked).name]));
        }
    };

    this.emptyQueues = function() {
        queuesTable.fnClearTable();
    };

    var removeTicketClicked = function(aRowData) {
        if(servicePoint.hasValidSettings()) {
            var removeParams = servicePoint.createParams();
            removeParams.queueId = sessvars.clickedQueueId;
            removeParams.visitId = aRowData.visitId;
            //WorkstationService.removeVisit(removeParams);
            servicePoint.callService("removeVisit", removeParams);
            queues.updateQueues(false);
        }
    };
	
	var callTicketClicked = function(aRowData) {
        if(servicePoint.hasValidSettings()) {
            var removeParams = servicePoint.createParams();
            removeParams.queueId = sessvars.clickedQueueId;
            removeParams.visitId = aRowData.visitId;
			userPoolUpdateNeeded = false;
			spPoolUpdateNeeded = false;
			sessvars.state = servicePoint.getState(servicePoint.callService("callVisit", removeParams));
			queues.updateQueues(false);
			if (sessvars.state.visitState == "CALL_NEXT_TO_QUICK") {
				util.showMessage(jQuery.i18n.prop("info.call.next.to.quick"));
			} else {
				sessvars.statusUpdated = new Date();
				servicePoint.updateWorkstationStatus();
				sessvars.currentCustomer = null;
			}
        }
    };
};