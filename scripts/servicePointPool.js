var servicePointPool = new function() {

    var servicePointPoolTable;

    this.updateServicePointPool = function() {
        if(typeof servicePointPoolTable !== 'undefined') {
            //empty the tickets table and populate with new data from server if table is not created
            servicePointPoolTable.fnClearTable();
            var params = servicePoint.createParams();
            //var tickets = WorkstationService.getVisitsInQueue(params);
            var tickets = servicePoint.callService("getVisitsInServicePointPool", params);
            servicePointPoolTable.fnAddData(tickets);
        } else {
            var columns = [
/* Id */                {"bSearchable": false,
                         "bVisible": false,
                         "mDataProp": "visitId"},
//        /* Parameters */        {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "parameters"},
//        /* Position */          {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "position"},
//        /* End time */          {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "endTime"},
//        /* Branch id */         {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "branchId"},
//        /* Service id */         {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "serviceId"},
//        /* Notes */             {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "notes"},
//        /* Customer id */       {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "customerId"},
//        /* Customer name */     {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "customerName"},
//        /* Nino */              {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "nino"},
          /* Ticket id */         {"sClass": "firstColumn",
                                    "mDataProp": "ticketId"},
//        /* Call time  */        {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "callTime"},
//        /* Ticket number */     {"sClass": "firstColumn",
//                                 "mDataProp": "ticketNumber"},
//        /* Service name */      {"sClass": "middleColumn",
//                                 "mDataProp": "serviceName"},
//        /* Move time */         {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "moveTime"},
//        /* Remove time */       {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "removeTime"},
//        /* App Id */            {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "appointmentId"},
//        /* Track number */      {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "trackNo"},
//        /* Queue id */          {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "queueId"},
//        /* Enter time */        {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "enterTime"},
        /* Waiting time */      {"sClass": "lastColumn",
                                 "mDataProp": "waitingTime"}
//        /* Staff id */          {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "staffId"},
//        /* Workstation id */    {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "workstationId"},
//        /* Confirm time */      {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "confirmTime"},
//        /* Create time */       {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "createTime"},
//        /* App time */          {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "appointmentTime"},
//        /* Close time */        {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "closeTime"},
//        /* Workstation name */  {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "workstationName"},
//        /* Workstation num */   {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "workstationNumber"},
//        /* Card data */         {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "cardData"}
            ];
            var headerCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                nHead.style.borderBottom = "1px solid #c0c0c0";
                nHead.getElementsByTagName('th')[0].innerHTML = jQuery.i18n.prop('info.servicepoint.pool.tickets');
                nHead.getElementsByTagName('th')[0].style.textAlign = "center";
                nHead.getElementsByTagName('th')[1].innerHTML = jQuery.i18n.prop('info.servicepoint.pool.waiting.time');
                nHead.getElementsByTagName('th')[1].style.textAlign = "center";
            };
            var url = "/rest/servicepoint/branches/" + sessvars.branchId + "/servicePoints/" + sessvars.servicePointId + "/pool/visits";
            var rowCallback = function(nRow, aData, iDisplayIndex) {
                //format ticket number
                if(!(servicePoint.isOutcomeOrDeliveredServiceNeeded() /*&& sessvars.forceMark && !hasMark()*/)) {
                    $('td:eq(0)', nRow).html("<span class='ticketNumSpan'>" + aData.ticketId + "</span>");
                } else {
                    $('td:eq(0)', nRow).addClass("ticketIdDisabled");
                }
                var formattedTime = util.formatIntoHHMM(parseInt(aData.waitingTime));
                $('td:eq(1)', nRow).html(formattedTime);
                $(nRow).addClass("");
                return nRow;
            };

            //create new table since not defined
            servicePointPoolTable = util.buildTableJson({"tableId": "servicePointPool", "url": url,
                "rowCallback": rowCallback, "columns":columns, "filter": false, "headerCallback": headerCallback,
                "scrollYHeight": "54px", "emptyTableLabel": "info.servicepoint.pool.no.tickets"});
        }

        //kill old event handlers
        $('tbody td span.ticketNumSpan', $('#servicePointPool')).die('click');

        //callbacks for calling, transferring and removing tickets
        $('tbody td span.ticketNumSpan', $('#servicePointPool')).live('click', function() {
            var nTr = $(this).closest("tr").get(0);
            var aData = servicePointPoolTable.fnGetData(nTr);
            ticketClicked(aData);
            return false;
        });

        $(document).ready(function() {
            var sorting = [[2, 'desc']];
            servicePointPoolTable.fnSort(sorting);
        });
    };

    var ticketClicked = function(aRowData) {
        if(servicePoint.hasValidSettings()) {
            var params = servicePoint.createParams();
            params.visitId = aRowData.visitId;
			userPoolUpdateNeeded = false;
            sessvars.state = servicePoint.getState(servicePoint.callService("callVisitFromServicePointPool", params));
            sessvars.statusUpdated = new Date();
            servicePoint.updateWorkstationStatus();
            sessvars.currentCustomer = null;
            customer.updateCustomerModule();
        }
    };

    this.isEmpty = function() {
        var isEmpty = true;
        var params = servicePoint.createParams();
        var tickets = servicePoint.callService("getVisitsInServicePointPool", params);
        if(typeof tickets !== 'undefined' && tickets != null && tickets.length > 0) {
            isEmpty = false;
        }
        return isEmpty;
    };

    this.emptyPool = function() {
        servicePointPoolTable.fnClearTable();
    }
};