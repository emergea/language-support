skillEnabled = true ;								// when enabled requires custom settings screens and calling rules

saveSettings = false;

var skills = new function() {

	var userAccount ;
	var userSkills = "" ;
	var servicePointSkills = "" ;
	var wsUnitId ="";
	var wsSkills ="";
	var newSkills ="";
	var skillRoles="";
	var numOfSkillRoles =-1;
	var servicePointPersonalQueue ="";
	var savedValue = "";


// on first load start here
// Get the users session info 
	this.skillInit = function() {
			var getParams = servicePoint.createParams();
	//		servicePoint.callServiceParse("getCurrentAccount", getParams, "skills.saveUserAccount");
			 skills.saveUserAccount(servicePoint.callServiceURL("/qsystem/rest/servicepoint/account"));
	}

// save the user account	 
	this.saveUserAccount = function(val) {
		userAccount=val;
		if (val != undefined) {
//		console.log ("account: " + userAccount.userName);
		var getParams = servicePoint.createParams();
		getParams.name= 'skill_'+userAccount.userName ;
		servicePoint.callServiceParse("getGlobalVariable", getParams, "skills.getSessionRoles");
		} else {
		init.init(); 
		}
	}

// extract the roles from the session parameters and get the last used skill set	 
	this.getSessionRoles = function(returnValue) {
		if (returnValue != undefined ) {
			savedValue = returnValue.value;
		}
		var m = userAccount.modules;

		for (i=0; i < m.length; i++){
			for (j=1; j<41; j++) {
			if (m[i] == "skill"+j) {
					if (skillRoles.length > 0) {
						skillRoles += ","+m[i]; 
					} else { 
						skillRoles += m[i];
					}
				}
			}
		}
		skillRoles = skillRoles.split(",");
		skillRoles.sort();
		skillRoles.reverse();
		numOfSkillRoles = skillRoles.length-1;
		
//		console.log (skillRoles);
		if (skillRoles.length > 0 && skillRoles!= "" ) {
			var getParams = servicePoint.createParams();
			getParams.name ="skillRoleUser"+(skillRoles[numOfSkillRoles].substring(5, skillRoles[numOfSkillRoles].length)) ;
			servicePoint.callServiceParse("getGlobalVariable", getParams, "skills.buildSkillSelect");
		} else {
			util.showError(jQuery.i18n.prop("error.no.available.skills"));
			init.init();  //init rest of workstationterminal
		}
	}
	
	this.resultParseDelay = function (val) {
		setTimeout(function() {skills.buildSkillSelect(val)}, 100);
	}
	
	
// from the role info we get the skill set and name and put it in a selection drop down inside the settings box
	this.buildSkillSelect = function (val) {
		var t = val.value;
		var select = document.getElementById("skillListModal");
		if(t.length > 0) {
			var opt = document.createElement("option");
			opt.value = t;
			t=t.split(",");
			opt.text = t[1];
			opt.name = t[1];
			try {
				select.add(opt, null); // standards compliant; doesn't work in IE
			} catch(ex) {
				select.add(opt); // IE only
			}
		}
		numOfSkillRoles = numOfSkillRoles-1;
		if (numOfSkillRoles != -1) {
		var getParams = servicePoint.createParams();
			getParams.name ="skillRoleUser"+(skillRoles[numOfSkillRoles].substring(5, skillRoles[numOfSkillRoles].length));
			servicePoint.callServiceParse("getGlobalVariable", getParams, "skills.resultParseDelay");
		} else {
			if (savedValue != "") {
				var t = savedValue.split("|");
				document.getElementById("skillListModal").value = t[0] ;
				}
//			console.log (savedValue + " Setting dropdown: " + t[0]);
			init.init();  //init rest of workstationterminal
		}
	}
	
// init of user skills ends here

// on apply settings start here	

// get workstation and execute call to branch variable to find the connected Service Point role
	this.getServicePointSkill = function(unitId) {
		wsUnitId=unitId;
		var getParams = servicePoint.createParams();
		getParams.branchId = sessvars.branchId ; 
		getParams.name = "roleServicePoints" ;
		if (sessvars.branchId != null ) {
			servicePoint.callServiceParse("getBranchVariable", getParams, "skills.getServicePointRoles");
		}
	}

	
//	extract the connect role and get the service point skills from the global variable
	this.getServicePointRoles = function(val) {
		if (val != undefined) {
			wsSkills = (val.value).split(",");
			var roleId;
			for (k=1; k < wsSkills.length;) {
				if (wsSkills[k] == wsUnitId) {
					roleId = wsSkills[k+1];
				}
				k = k+2;
			}
			var getParams = servicePoint.createParams();
			getParams.name = "skillRoleWs"+roleId ;
			servicePoint.callServiceParse("getGlobalVariable", getParams, "skills.getServicepointQueues");
		} else {
			util.showMessage ("No Skills set for this Service Point, you will not be able to call any customers");
		}
	}

	
// get the skills for the workstation and workstation role	
	this.getServicepointQueues = function(val) {
	
		if (val != undefined) {
			servicePointSkills = val.value ;
		} else {
			util.showMessage ("No Skills set for this Service Point Role, you will not be able to call any customers");
		}
		var t = document.getElementById("skillListModal").value;

		if (t != undefined ) {
			userSkills = t;
		} else {
			util.showMessage ("No Skills set for your Role/Service Point combination, you will not be able to call any customers!");
		}
		var getParams = servicePoint.createParams();
			getParams.branchId = sessvars.branchId ;
			getParams.name = "queueServicePoints" ;
			if (sessvars.branchId != null) {
				servicePoint.callServiceParse("getBranchVariable", getParams, "skills.getPersonalQueue");
			}
	}
	
	// get the personal queue for the workstation	
	this.getPersonalQueue = function(val) {
		servicePointPersonalQueue
		if (val != undefined) {

//console.log ( "Workstation personal queues = " + val.value);
			var t = (val.value).split(",") ;
			for (i=1; i < t.length ; i=i+2) {
				if (t[i] == wsUnitId) {
					servicePointPersonalQueue = t[i+1] ;
				}
			}	
		} 
		skills.setSkillsToSession();
	}

// combine the workstation skills and selected users skills into a new skill set and save it in the session parameter	
	this.setSkillsToSession = function() {
	
//console.log (servicePointPersonalQueue );
	if (servicePointPersonalQueue == "") {
		newSkills = "";
		} else {
			newSkills = servicePointPersonalQueue+",2";
		}
		var u = userSkills.split(',');
		var s = servicePointSkills.split(',');	
		for (i=2; i < u.length ; i=i+2) {
			for (j=2; j<s.length ; j=j+2) {
				if (u[i] == s[j]) {
					if (newSkills.length > 0) {
						newSkills += ',';
					}
					newSkills += (u[i] + ',' + u[i+1]);
					
				}
			}
		}


		queueSkills = "";
		var w = newSkills.split(",") ;

		for (i=0; i < w.length ; i=i+2) {
			if (i==0) {
				queueSkills = w[i];
			} else {
				queueSkills += "," + w[i];
			}
		}
//		console.log ("------------" +savedValue);
		sessvars.skillQueues = queueSkills ;
		if (newSkills.length == 0) {
			util.showMessage ( "No skills available for current Staff Member/Service Point, you will not be able to call any customers!");
		}
// save the skills in the user session
		var params = {};
		params.branchId = sessvars.branchId ;
		params.userName = sessvars.currentUser.userName;
		params.$entity = {
				'name': 'skills',
				'value': newSkills
			};
		var t= servicePoint.callService("setUserSessionParameter", params);
		
		if (saveSettings == true) {
		saveSettings = false;
// save the skills in a variable		
		savedValue = document.getElementById("skillListModal").value + "|" + newSkills ;
		var setParams = servicePoint.createParams();
			setParams.$entity = {
				'name': 'skill_'+userAccount.userName ,
				'value': savedValue
			};
		servicePoint.callService("setGlobalVariable", setParams);
		}
//		console.log ( "Saved in variables = " +savedValue);
//		console.log ( "Queues to be shown = " + queueSkills);	
//		console.log ( "Skill set saved in session = " + newSkills);	
		var t = savedValue.split("|");

	}
	
	this.cancelChangeSkill = function() {
		var t = savedValue.split("|");
			document.getElementById("skillListModal").value = t[0] ;
//			console.log ("dropdown has been set back to previous selected skill");
	}
};

