var transfer = new function() {
    var transferTable;
    var transferToStaffPoolTable;
    var transferToServicePointPoolTable;

    var transferTicketToQueueTable;
    var transferQueueToStaffPoolTable;
    var transferQueueToServicePointPoolTable;

    this.transferPressed = function() {
        if(servicePoint.hasValidSettings() && sessvars.state.userState == servicePoint.userState.SERVING) {
            util.showModal("transfers");

            // Transfer to queue
            if(typeof transferTable !== 'undefined') {
                transferTable.fnClearTable();
                var queues = servicePoint.callService("getQueues", servicePoint.createParams());
                transferTable.fnAddData(queues);
                transferTable.fnAdjustColumnSizing();
            } else {
                var columns = [
                    /* Queue name */        {"sClass": "firstColumn",
                        "mDataProp": "name"},
                    /* Actions */      {"sClass": "middleColumn",
                        "mData": null,
                        "sDefaultContent": ""},
                    /* Queue id */          {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "id"},
                    /* Queue waiting time */{"sClass": "middleColumn",
                        "mDataProp": "waitingTime"},
                    /* Queue waiting num */ {"sClass": "lastColumn",
                        "mDataProp": "customersWaiting"}
                ];
                var url = "/rest/servicepoint/branches/" + sessvars.branchId + "/queues";
                var headerCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                    $(nHead).closest('thead, THEAD').find('.transferQueueHeader').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.queue.header'));
                        $(item).addClass('subheader');
                    });
                    $(nHead).closest('thead, THEAD').find('.transferQueueName').each( function (i, item) {
                        $(item).parent().css('borderBottom', "1px solid #c0c0c0");
                        $(item).html(jQuery.i18n.prop('info.transfer.queue.name'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferQueueActions').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.queue.actions'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferQueueWaitingTime').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.queue.waiting.time'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferQueueCustomerWaiting').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.queue.waiting'));
                    });
                };
                var rowCallback = function(nRow, aData, iDisplayIndex) {
                    if($('td:eq(0)', nRow).find('span').length == 0) {
                        var queueName = $('td:eq(0)', nRow).text();
                        $('td:eq(0)', nRow).empty().append("<span class=\"queueNameSpan\">" + queueName + "</span>");
						if ( buttonTransferFirstEnabled  == true ) {						
							$('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"transferTicketFirst\" title=\"" +
								translate.msg("action.title.transfer.first", [sessvars.state.visit.ticketId]) + "\"></a></span>");
						}
						if ( buttonTransferLastEnabled  == true ) {
							$('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"transferTicketLast\" title=\"" +
								translate.msg("action.title.transfer.last", [sessvars.state.visit.ticketId]) + "\"></a></span>");
						}
						if ( buttonTransferSortEnabled  == true ) {							
							$('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"transferTicketSort\" title=\"" +
								translate.msg("action.title.transfer.sorted", [sessvars.state.visit.ticketId]) + "\"></a></span>");
						}
                    }
                    $('td:eq(2)', nRow).html(util.formatIntoHHMM(parseInt(aData.waitingTime)));
                    return nRow;
                };
                transferTable = util.buildTableJson({"tableId": "transferQueues", "url": url, "rowCallback": rowCallback,
                    "columns": columns, "filter": false, "headerCallback": headerCallback, "scrollYHeight": "150px",
                    "emptyTableLabel":"info.transfer.queue.empty"});
            }
            //destroy old event handlers
			if ( buttonTransferFirstEnabled  == true ) {			
				$('tbody tr td span a.transferTicketFirst', $('#transferQueues')).die('click');
			}
			if ( buttonTransferLastEnabled  == true ) {
				$('tbody tr td span a.transferTicketLast', $('#transferQueues')).die('click');
			}
			if ( buttonTransferSortEnabled  == true ) {
				$('tbody tr td span a.transferTicketSort', $('#transferQueues')).die('click');
			}
            //make new ones
			if ( buttonTransferFirstEnabled  == true ) {	
				$('tbody tr td span a.transferTicketFirst', $('#transferQueues')).live('click',function(){
					var nTr = $(this).closest("tr").get(0);
					var aData = transferTable.fnGetData(nTr);
					transferCurrentVisitToQueueClicked("FIRST", aData);
				});
			}
			if ( buttonTransferLastEnabled  == true ) {	
				$('tbody tr td span a.transferTicketLast', $('#transferQueues')).live('click',function(){
					var nTr = $(this).closest("tr").get(0);
					var aData = transferTable.fnGetData(nTr);
					transferCurrentVisitToQueueClicked("LAST", aData);
				});
			}
			if ( buttonTransferSortEnabled  == true ) {	
				$('tbody tr td span a.transferTicketSort', $('#transferQueues')).live('click',function(){
					var nTr = $(this).closest("tr").get(0);
					var aData = transferTable.fnGetData(nTr);
					transferCurrentVisitToQueueClicked("SORTED", aData);
				});
			}
            // Transfer to staff pool
            if(typeof transferToStaffPoolTable !== 'undefined') {
                transferToStaffPoolTable.fnClearTable();
                //var services = WorkstationService.getServices({'branchId': sessvars.branchId});
                var callParams = servicePoint.createParams();
                callParams.onlyServicePoints = 'true';
                var users = servicePoint.callService("getLoggedInUsers", callParams);
                transferToStaffPoolTable.fnAddData(users);
                transferToStaffPoolTable.fnAdjustColumnSizing();
            } else {
                var staffPoolColumns = [
                    /* Id */         {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "id"},
                    /* User name  */ {"sClass": "firstColumn",
                        "mDataProp": "userName"},
                    /* Actions */      {"sClass": "middleColumn",
                        "mData": null,
                        "sDefaultContent": ""},
                    /* First mme */  {"sClass": "middleColumn",
                        "mDataProp": "firstName"},
                    /* Last name */  {"sClass": "lastColumn",
                        "mDataProp": "lastName"},
                    /* Locale */     {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "locale"},
                    /* Direction */  {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "direction"}
                ];
                var staffPoolUrl = "/rest/servicepoint/branches/" + sessvars.branchId + "/users;onlyServicePoints=true";
                var staffPoolHeaderCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                    $(nHead).closest('thead, THEAD').find('.transferStaffPoolHeader').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.staff.pool.header'));
                        $(item).addClass('subheader');
                    });
                    $(nHead).closest('thead, THEAD').find('.transferStaffPoolUserName').each( function (i, item) {
                        $(item).parent().css('borderBottom', "1px solid #c0c0c0");
                        $(item).html(jQuery.i18n.prop('info.transfer.staff.pool.username'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferStaffPoolActions').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.staff.pool.actions'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferStaffPoolFirstName').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.staff.pool.firstname'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferStaffPoolLastName').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.staff.pool.lastname'));
                    });
                };
                var staffPoolRowCallback = function(nRow, aData, iDisplayIndex) {
                    if($('td:eq(0)', nRow).find('span').length == 0) {
                        var staffName = $('td:eq(0)', nRow).text();
                        $('td:eq(0)', nRow).empty().append("<span class=\"staffNameSpan\">" + staffName + "</span>");
                        $('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"transferTicketFirst\" title=\"" +
                            translate.msg("action.title.transfer.staff.pool", [sessvars.state.visit.ticketId, staffName]) + "\"></a></span>");
                    }
                    return nRow;
                };
                transferToStaffPoolTable = util.buildTableJson({"tableId": "transferTicketToStaffPoolTable",
                    "url": staffPoolUrl, "rowCallback": staffPoolRowCallback, "columns": staffPoolColumns,
                    "filter": false, "headerCallback": staffPoolHeaderCallback, "scrollYHeight": "150px",
                    "emptyTableLabel":"info.transfer.staff.pool.empty"});
            }
            //destroy old event handlers
            $('tbody tr td span a.transferTicketFirst', $('#transferTicketToStaffPoolTable')).die('click');
            //make new ones
            $('tbody tr td span a.transferTicketFirst', $('#transferTicketToStaffPoolTable')).live('click',function(){
                var nTr = $(this).closest("tr").get(0);
                var aData = transferToStaffPoolTable.fnGetData(nTr);
                transferCurrentVisitToStaffPoolClicked("FIRST", aData);
            });

            // Transfer to service point pool
            if(typeof transferToServicePointPoolTable !== 'undefined') {
                transferToServicePointPoolTable.fnClearTable();
                //var services = WorkstationService.getServices({'branchId': sessvars.branchId});
                var params = servicePoint.createParams();
                params.deviceType = "SW_SERVICE_POINT";
                var servicePoints = servicePoint.callService("getServicePointsByDeviceType", params);
                transferToServicePointPoolTable.fnAddData(servicePoints);
                transferToServicePointPoolTable.fnAdjustColumnSizing();
            } else {
                var servicePointColumns = [
                    /* Name */        {"sClass": "firstColumn",
                        "mDataProp": "name"},
                    /* Actions */      {"sClass": "middleColumn",
                        "mData": null,
                        "sDefaultContent": ""},
                    /* Id */          {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "id"},
                    /* Unit id */     {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "unitId"},
                    /* State*/{"sClass": "lastColumn",
                        "mDataProp": "state"},
                    /* Parameters */ {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "parameters"}
                ];
                var servicePointUrl = "/rest/servicepoint/branches/" + sessvars.branchId + "/servicePoints/deviceTypes/SW_SERVICE_POINT";
                var servicePointHeaderCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                    $(nHead).closest('thead, THEAD').find('.transferServicePointPoolHeader').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.servicepoint.pool.header'));
                        $(item).addClass('subheader');
                    });
                    $(nHead).closest('thead, THEAD').find('.transferServicePointPoolName').each( function (i, item) {
                        $(item).parent().css('borderBottom', "1px solid #c0c0c0");
                        $(item).html(jQuery.i18n.prop('info.transfer.servicepoint.pool.name'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferServicePointPoolActions').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.servicepoint.pool.actions'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferServicePointPoolState').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.servicepoint.pool.state'));
                    });
                };
                var servicePointRowCallback = function(nRow, aData, iDisplayIndex) {
                    if($('td:eq(0)', nRow).find('span').length == 0) {
                        var servicePointName = $('td:eq(0)', nRow).text();
                        $('td:eq(0)', nRow).empty().append("<span class=\"servicePointNameSpan\">" + servicePointName +
                            "</span>");
                        $('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"transferTicketFirst\" title=\"" +
                            translate.msg("action.title.transfer.servicepoint.pool", [sessvars.state.visit.ticketId, servicePointName]) + "\"></a></span>");
                    }
                    $('td:eq(2)', nRow).html(translate.msg("info.transfer.servicepoint.pool.state." + aData.state));
                    return nRow;
                };
                transferToServicePointPoolTable = util.buildTableJson({"tableId": "transferTicketToServicePointPoolTable",
                    "url": servicePointUrl, "rowCallback": servicePointRowCallback, "columns": servicePointColumns,
                    "filter": false, "headerCallback": servicePointHeaderCallback, "scrollYHeight": "150px",
                    "emptyTableLabel":"info.transfer.servicepoint.pool.empty"});
            }
            //destroy old event handlers
            $('tbody tr td span a.transferTicketFirst', $('#transferTicketToServicePointPoolTable')).die('click');
            //make new ones
            $('tbody tr td span a.transferTicketFirst', $('#transferTicketToServicePointPoolTable')).live('click',function(){
                var nTr = $(this).closest("tr").get(0);
                var aData = transferToServicePointPoolTable.fnGetData(nTr);
                transferCurrentVisitToServicePointPoolClicked("FIRST", aData);
            });
        }
    };

    var transferCurrentVisitToQueueClicked = function(sortType, rowData) {
        if(sessvars.state.userState == servicePoint.userState.SERVING) {
//            transferParams.newServiceId = rowData.id; //using value of mDataProp to retrieve the service id
//            transferParams.policy = sortType;
//            transferParams.delay = 0; //the time in minutes that must pass before the ticket can be called again
//            //sessvars.state = WorkstationService.transferVisitWorkstationToService(transferParams);
//            sessvars.state = servicePoint.getState(servicePoint.callService("transferVisitWorkstationToService", transferParams));

            var transferParams = servicePoint.createParams();
            transferParams.queueId = rowData.id;
            transferParams.$entity = {
                "fromId" : sessvars.servicePointId,
                "fromBranchId" : sessvars.branchId,
                "visitId": sessvars.state.visit.id,
                "sortPolicy" : sortType
            };
			 transferParams.json='{"fromId":'+ sessvars.servicePointId + ',"fromBranchId":'+ sessvars.branchId + ',"visitId":' + sessvars.state.visit.id + ',"sortPolicy":"'+sortType + '"}';			
         
        //    sessvars.state = servicePoint.getState(servicePoint.callService("transferVisitToQueue", transferParams));
			sessvars.state = servicePoint.getState(spService.transferVisitToQueue(transferParams));
            sessvars.statusUpdated = new Date();
            servicePoint.updateWorkstationStatus(false);
            if(sessvars.state.userState == servicePoint.userState.NO_STARTED_USER_SESSION) {
                util.showError(jQuery.i18n.prop("error.not.loggedin"));
                util.hideModal("transfers");
                return;
            }
            sessvars.currentCustomer = null;
            sessvars.cfuSelectionSet = true;
            customer.updateCustomerModule();
        } else {
            util.showError(jQuery.i18n.prop("error.no.ongoing.transaction"));
        }
        util.hideModal("transfers");
    };

    //transfer current visit to new service
    var transferCurrentVisitToServicePointPoolClicked = function(sortType, rowData) {
        if(sessvars.state.userState == servicePoint.userState.SERVING) {

            var transferParams = servicePoint.createParams();
            transferParams.servicePointId = rowData.id;
            transferParams.$entity = {
                "fromId" : sessvars.servicePointId,
                "fromBranchId" : sessvars.branchId,
                "visitId": sessvars.state.visit.id
//                "sortPolicy" : sortType
            };
			userPoolUpdateNeeded = false;
			transferParams.json='{"fromId":'+ sessvars.servicePointId + ',"fromBranchId":'+ sessvars.branchId + ',"visitId":' + sessvars.state.visit.id + '}';			
 			sessvars.state = servicePoint.getState(spService.transferVisitToServicePointPool(transferParams));
         //   sessvars.state = servicePoint.getState(servicePoint.callService("transferVisitToServicePointPool", transferParams));

            sessvars.statusUpdated = new Date();
            servicePoint.updateWorkstationStatus(false);
            if(sessvars.state.userState == servicePoint.userState.NO_STARTED_USER_SESSION) {
                util.showError(jQuery.i18n.prop("error.not.loggedin"));
                util.hideModal("transfers");
                return;
            }
            sessvars.currentCustomer = null;
            customer.updateCustomerModule();
        } else {
            util.showError(jQuery.i18n.prop("error.no.ongoing.transaction"));
        }
        util.hideModal("transfers");
    };

    var transferCurrentVisitToStaffPoolClicked = function(sortType, rowData) {
        if(sessvars.state.userState == servicePoint.userState.SERVING) {

            var transferParams = servicePoint.createParams();
            transferParams.userId = rowData.id;
            transferParams.$entity = {
                "fromId" : sessvars.servicePointId,
                "fromBranchId" : sessvars.branchId,
                "visitId": sessvars.state.visit.id
//                "sortPolicy" : sortType
            };
			spPoolUpdateNeeded = false;
			transferParams.json='{"fromId":'+ sessvars.servicePointId + ',"fromBranchId":'+ sessvars.branchId + ',"visitId":' + sessvars.state.visit.id + '}';			
 			sessvars.state = servicePoint.getState(spService.transferVisitToUserPool(transferParams));

   //         sessvars.state = servicePoint.getState(servicePoint.callService("transferVisitToUserPool", transferParams));

            sessvars.statusUpdated = new Date();
            servicePoint.updateWorkstationStatus(false);
            if(sessvars.state.userState == servicePoint.userState.NO_STARTED_USER_SESSION) {
                util.showError(jQuery.i18n.prop("error.not.loggedin"));
                util.hideModal("transfers");
                return;
            }
            sessvars.currentCustomer = null;
            customer.updateCustomerModule();
        } else {
            util.showError(jQuery.i18n.prop("error.no.ongoing.transaction"));
        }
        util.hideModal("transfers");
    };

    this.hideTransfers = function() {
        util.hideModal("transfers");
    };

    this.cancelTransfer = function() {
        util.hideModal("transfers");
    };

    //transfer clicked in ticket list => open a new dialogue presenting a list of services and transfer options for each service
    this.transferTicketToQueueClicked = function(aRowData) {
        if(servicePoint.hasValidSettings()) {
            // ugly but working. used in the row callback to put the ticket number in the header.
            sessvars.ticketIdToTransfer = aRowData.ticketId;
            util.showModal("transferQueueToQueueDialogue");
            //need to store some information from the tickets table for later usage, when calling/transferring a ticket
//            sessvars.visitId = aRowData.visitId; //ticket track number
//            sessvars.ticketToTransferServiceId = aRowData.serviceId; //old service id
            if(typeof transferTicketToQueueTable != 'undefined') {
                transferTicketToQueueTable.fnClearTable();
                //var services = WorkstationService.getServices({'branchId': sessvars.branchId});
                var queues = servicePoint.callService("getQueues", {'branchId': sessvars.branchId});
                transferTicketToQueueTable.fnAddData(queues);
                transferTicketToQueueTable.fnAdjustColumnSizing();
            } else {
                var columns = [
                    /* Queue name */        {"sClass": "firstColumn",
                        "mDataProp": "name"},
                    /* Actions */      {"sClass": "middleColumn",
                        "mData": null,
                        "sDefaultContent": ""},
                    /* Queue id */          {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "id"},
//        /* Queue description */ {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "description"},
//        /* Queue branch id */   {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "branchId"},
//        /* Queue QL num */      {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "queueLogicNumber"},
                    /* Queue waiting time */{"sClass": "middleColumn",
                        "mDataProp": "waitingTime"},
                    /* Queue waiting num */ {"sClass": "lastColumn",
                        "mDataProp": "customersWaiting"}
//        /* Queue letter */      {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "letter"},
//        /* Queue est wait */    {"bSearchable": false,
//                                 "bVisible": false,
//                                 "mDataProp": "estimatedWait"}
                ];
                var url = "/rest/servicepoint/branches/" + sessvars.branchId + "/queues";
                var headerCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                    $(nHead).closest('thead, THEAD').find('.transferQueueHeader').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.queue.header'));
                        $(item).addClass('subheader');
                    });
                    $(nHead).closest('thead, THEAD').find('.transferQueueName').each( function (i, item) {
                        $(item).parent().css('borderBottom', "1px solid #c0c0c0");
                        $(item).html(jQuery.i18n.prop('info.transfer.queue.name'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferQueueActions').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.queue.actions'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferQueueWaitingTime').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.queue.waiting.time'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferQueueCustomerWaiting').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.queue.waiting'));
                    });
                };
                var rowCallback = function(nRow, aData, iDisplayIndex) {
                    if($('td:eq(0)', nRow).find('span').length == 0) {
                        var queueName = $('td:eq(0)', nRow).text();
                        $('td:eq(0)', nRow).empty().append("<span class=\"queueNameSpan\">" + queueName + "</span>");
						if ( buttonTransferFirstEnabled  == true ) {	
							$('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"transferTicketFirst\" title=\"" +
								translate.msg("action.title.transfer.first", [sessvars.ticketIdToTransfer]) + "\"></a></span>");
						}
						if ( buttonTransferLastEnabled  == true ) {	
							$('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"transferTicketLast\" title=\"" +
								translate.msg("action.title.transfer.last", [sessvars.ticketIdToTransfer]) + "\"></a></span>");
						}
						if ( buttonTransferSortEnabled  == true ) {	
							$('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"transferTicketSort\" title=\"" +
								translate.msg("action.title.transfer.sorted", [sessvars.ticketIdToTransfer]) + "\"></a></span>");
						}
                    }
                    $('td:eq(2)', nRow).html(util.formatIntoHHMMSS(parseInt(aData.waitingTime)));
                    return nRow;
                };
                transferTicketToQueueTable = util.buildTableJson({"tableId": "transferTicketQueueToQueueTable", "url": url,
                    "rowCallback": rowCallback, "columns": columns, "filter": false, "headerCallback": headerCallback,
                    "scrollYHeight": "150px", "emptyTableLabel":"info.transfer.queue.empty"});
                //transferTicketToQueueTable = util.buildTable("transferTicketQueueToQueueTable", url, rowCallback, columns, false, headerCallback, true);
            }
            //destroy old event handlers
			if ( buttonTransferFirstEnabled  == true ) {	
				$('tbody td a.transferTicketFirst', $('#transferTicketQueueToQueueTable')).die('click');
			}
			if ( buttonTransferLastEnabled  == true ) {	
				$('tbody td a.transferTicketLast', $('#transferTicketQueueToQueueTable')).die('click');
			}
			if ( buttonTransferSortEnabled  == true ) {	
				$('tbody td a.transferTicketSort', $('#transferTicketQueueToQueueTable')).die('click');
			}
            //make new ones
			if ( buttonTransferFirstEnabled  == true ) {	
				$('tbody td a.transferTicketFirst', $('#transferTicketQueueToQueueTable')).live('click',function(){
					var nTr = $(this).closest("tr").get(0);
					var aData = transferTicketToQueueTable.fnGetData(nTr);
					transferTicketToQueue("FIRST", aData, aRowData.visitId);
				});
			}
			if ( buttonTransferLastEnabled  == true ) {	
				$('tbody td a.transferTicketLast', $('#transferTicketQueueToQueueTable')).live('click',function(){
					var nTr = $(this).closest("tr").get(0);
					var aData = transferTicketToQueueTable.fnGetData(nTr);
					transferTicketToQueue("LAST", aData, aRowData.visitId);
				});
			}
			if ( buttonTransferSortEnabled  == true ) {	
				$('tbody td a.transferTicketSort', $('#transferTicketQueueToQueueTable')).live('click',function(){
					var nTr = $(this).closest("tr").get(0);
					var aData = transferTicketToQueueTable.fnGetData(nTr);
					transferTicketToQueue("SORTED", aData, aRowData.visitId);
				});
			}

            // Transfer to staff pool
            if(typeof transferQueueToStaffPoolTable != 'undefined') {
                transferQueueToStaffPoolTable.fnClearTable();
                //var services = WorkstationService.getServices({'branchId': sessvars.branchId});
                var callParams = servicePoint.createParams();
                callParams.onlyServicePoints = 'true';
                var users = servicePoint.callService("getLoggedInUsers", callParams);
                transferQueueToStaffPoolTable.fnAddData(users);
                transferQueueToStaffPoolTable.fnAdjustColumnSizing();
            } else {
                var staffPoolColumns = [
                    /* Id */         {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "id"},
                    /* User name  */ {"sClass": "firstColumn",
                        "mDataProp": "userName"},
                    /* Actions */      {"sClass": "middleColumn",
                        "mData": null,
                        "sDefaultContent": ""},
                    /* First name */ {"sClass": "middleColumn",
                        "mDataProp": "firstName"},
                    /* Last name */  {"sClass": "lastColumn",
                        "mDataProp": "lastName"},
                    /* Locale */     {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "locale"},
                    /* Direction */  {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "direction"}
                ];
                var staffPoolUrl = "/rest/servicepoint/branches/" + sessvars.branchId + "/users;onlyServicePoints=true";
                var staffPoolHeaderCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                    $(nHead).closest('thead, THEAD').find('.transferStaffPoolHeader').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.staff.pool.header'));
                        $(item).addClass('subheader');
                    });
                    $(nHead).closest('thead, THEAD').find('.transferStaffPoolUserName').each( function (i, item) {
                        $(item).parent().css('borderBottom', "1px solid #c0c0c0");
                        $(item).html(jQuery.i18n.prop('info.transfer.staff.pool.username'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferStaffPoolActions').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.staff.pool.actions'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferStaffPoolFirstName').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.staff.pool.firstname'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferStaffPoolLastName').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.staff.pool.lastname'));
                    });
                };
                var staffPoolRowCallback = function(nRow, aData, iDisplayIndex) {
                    if($('td:eq(0)', nRow).find('span').length == 0) {
                        var staffName = $('td:eq(0)', nRow).text();
                        $('td:eq(0)', nRow).empty().append("<span class=\"staffNameSpan\">" + staffName + "</span>");
                        $('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"transferTicketFirst\" title=\"" +
                            translate.msg("action.title.transfer.staff.pool", [sessvars.ticketIdToTransfer, staffName]) + "\"></a></span>");
                    }
                    return nRow;
                };
                transferQueueToStaffPoolTable = util.buildTableJson({"tableId": "transferQueueToStaffPoolTable",
                    "url": staffPoolUrl, "rowCallback": staffPoolRowCallback, "columns": staffPoolColumns,
                    "filter": false, "headerCallback": staffPoolHeaderCallback, "scrollYHeight": "150px",
                    "emptyTableLabel":"info.transfer.staff.pool.empty"});
            }
            //destroy old event handlers
            $('tbody tr td span a.transferTicketFirst', $('#transferQueueToStaffPoolTable')).die('click');
            //make new ones
            $('tbody tr td span a.transferTicketFirst', $('#transferQueueToStaffPoolTable')).live('click',function(){
                var nTr = $(this).closest("tr").get(0);
                var aData = transferQueueToStaffPoolTable.fnGetData(nTr);
                transferVisitInQueueToStaffPoolClicked("FIRST", aData, aRowData.visitId);
            });

            // Transfer to service point pool
            if(typeof transferQueueToServicePointPoolTable != 'undefined') {
                transferQueueToServicePointPoolTable.fnClearTable();
                //var services = WorkstationService.getServices({'branchId': sessvars.branchId});
                var params = servicePoint.createParams();
                params.deviceType = "SW_SERVICE_POINT";
                var servicePoints = servicePoint.callService("getServicePointsByDeviceType", params);
                transferQueueToServicePointPoolTable.fnAddData(servicePoints);
                transferQueueToServicePointPoolTable.fnAdjustColumnSizing();
            } else {
                var servicePointColumns = [
                    /* Name */        {"sClass": "firstColumn",
                        "mDataProp": "name"},
                    /* Actions */      {"sClass": "middleColumn",
                        "mData": null,
                        "sDefaultContent": ""},
                    /* Id */          {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "id"},
                    /* Unit id */     {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "unitId"},
                    /* State*/{"sClass": "lastColumn",
                        "mDataProp": "state"},
                    /* Parameters */ {"bSearchable": false,
                        "bVisible": false,
                        "mDataProp": "parameters"}
                ];
                var servicePointUrl = "/rest/servicepoint/branches/" + sessvars.branchId + "/servicePoints/deviceTypes/SW_SERVICE_POINT";
                var servicePointHeaderCallback = function(nHead, aasData, iStart, iEnd, aiDisplay) {
                    $(nHead).closest('thead, THEAD').find('.transferServicePointPoolHeader').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.servicepoint.pool.header'));
                        $(item).addClass('subheader');
                    });
                    $(nHead).closest('thead, THEAD').find('.transferServicePointPoolName').each( function (i, item) {
                        $(item).parent().css('borderBottom', "1px solid #c0c0c0");
                        $(item).html(jQuery.i18n.prop('info.transfer.servicepoint.pool.name'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferServicePointPoolActions').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.servicepoint.pool.actions'));
                    });
                    $(nHead).closest('thead, THEAD').find('.transferServicePointPoolState').each( function (i, item) {
                        $(item).html(jQuery.i18n.prop('info.transfer.servicepoint.pool.state'));
                    });
                };
                var servicePointRowCallback = function(nRow, aData, iDisplayIndex) {
                    if($('td:eq(0)', nRow).find('span').length == 0) {
                        var servicePointName = $('td:eq(0)', nRow).text();
                        $('td:eq(0)', nRow).empty().append("<span class=\"servicePointNameSpan\">" + servicePointName +
                            "</span>");
                        $('td:eq(1)', nRow).append("<span><a href=\"#\" class=\"transferTicketFirst\" title=\"" +
                            translate.msg("action.title.transfer.servicepoint.pool", [sessvars.ticketIdToTransfer, servicePointName]) + "\"></a></span>");
                    }
                    $('td:eq(2)', nRow).html(translate.msg("info.transfer.servicepoint.pool.state." + aData.state));
                    return nRow;
                };
                transferQueueToServicePointPoolTable = util.buildTableJson({"tableId": "transferQueueToServicePointPoolTable",
                    "url": servicePointUrl, "rowCallback": servicePointRowCallback, "columns": servicePointColumns,
                    "filter": false, "headerCallback": servicePointHeaderCallback, "scrollYHeight": "150px",
                    "emptyTableLabel":"info.transfer.servicepoint.pool.empty"});
            }
            //destroy old event handlers
            $('tbody tr td span a.transferTicketFirst', $('#transferQueueToServicePointPoolTable')).die('click');
            //make new ones
            $('tbody tr td span a.transferTicketFirst', $('#transferQueueToServicePointPoolTable')).live('click',function(){
                var nTr = $(this).closest("tr").get(0);
                var aData = transferQueueToServicePointPoolTable.fnGetData(nTr);
                transferVisitInQueueToServicePointPoolClicked("FIRST", aData, aRowData.visitId);
            });
        }
    };

    //transfer icon pressed
    var transferTicketToQueue = function(sortType, aRowData, visitId) {
        if(servicePoint.hasValidSettings()) {
            var transferParams = servicePoint.createParams();
//            transferParams.oldServiceId = sessvars.ticketToTransferServiceId;
//            transferParams.trackNumber = sessvars.ticketToTransferTrackNumber;
            transferParams.queueId = aRowData.id;
            transferParams.$entity = {
                "fromId": sessvars.servicePointId,
                "fromBranchId" : sessvars.branchId,
                "visitId": visitId,
                "sortPolicy" : sortType
            };

//            servicePoint.callService("transferVisitToQueue", transferParams);
			transferParams.json='{"fromId":'+ sessvars.servicePointId + ',"fromBranchId":'+ sessvars.branchId + ',"visitId":' + visitId + ',"sortPolicy":"'+sortType + '"}';			
			spService.transferVisitToQueue(transferParams);
			queues.updateQueues();
            util.hideModal("transferQueueToQueueDialogue");
        }
    };

    var transferVisitInQueueToStaffPoolClicked = function(sortType, aRowData, visitId) {
        if(servicePoint.hasValidSettings()) {

            var transferParams = servicePoint.createParams();
            transferParams.userId = aRowData.id;
            transferParams.$entity = {
                "fromId" : sessvars.servicePointId,
                "fromBranchId" : sessvars.branchId,
                "visitId": visitId
//                "sortPolicy" : sortType
            };
			spPoolUpdateNeeded = false;
            transferParams.json='{"fromId":'+ sessvars.servicePointId + ',"fromBranchId":'+ sessvars.branchId + ',"visitId":' + visitId + '}';			
         //   servicePoint.callService("transferVisitToUserPool", transferParams);
			spService.transferVisitToUserPool(transferParams);
            util.hideModal("transferQueueToQueueDialogue");
        }
    };

    var transferVisitInQueueToServicePointPoolClicked = function(sortType, aRowData, visitId) {
        if(servicePoint.hasValidSettings()) {
            var transferParams = servicePoint.createParams();
            transferParams.servicePointId = aRowData.id;
            transferParams.$entity = {
                "fromId" : sessvars.servicePointId,
                "fromBranchId" : sessvars.branchId,
                "visitId": visitId
//                "sortPolicy" : sortType
            };
			userPoolUpdateNeeded = false;
            transferParams.json='{"fromId":'+ sessvars.servicePointId + ',"fromBranchId":'+ sessvars.branchId + ',"visitId":' + visitId + '}';			
        //    servicePoint.callService("transferVisitToServicePointPool", transferParams);
			spService.transferVisitToServicePointPool(transferParams);
            util.hideModal("transferQueueToQueueDialogue");
        }
    };
};