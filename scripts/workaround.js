// should be removed after release of 5.3

// PUT /servicepoint/branches/{branchId}/users/{userName}/sessionVariables
ServicePointService.setUserSessionParameter = function(_params){
 var params = _params ? _params : {};
 var request = new REST.Request();
 request.setMethod('PUT');
 var uri = params.$apiURL ? params.$apiURL : REST.apiURL;
 uri += '/servicepoint/branches/';
 uri += REST.Encoding.encodePathSegment(params.branchId);
 uri += '/users/';
 uri += REST.Encoding.encodePathSegment(params.userName);
 uri += '/sessionVariables';
 if(params.$entity)
  request.setEntity(params.$entity);
 request.setURI(uri);
 if(params.$username && params.$password)
  request.setCredentials(params.$username, params.$password);
 if(params.$accepts)
  request.setAccepts(params.$accepts);

 if(params.$contentType)
  request.setContentType(params.$contentType);
 else
  request.setContentType('application/json');
 if(params.$callback){
  request.execute(params.$callback);
 }else{
  var returnValue;
  request.setAsync(false);
  var callback = function(httpCode, xmlHttpRequest, value){ returnValue = value;};
  request.execute(callback);
  return returnValue;
 }
};


